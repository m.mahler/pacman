/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


window.onload = (function() {
    Crafty.init(32 * 20, 32 * 20);
    Crafty.canvas.init();
    var WIDTH = 640,
            HEIGHT = 640,
            BOARD_COLS = 20,
            BOARD_ROWS = 20,
            TILEW = 32,
            TILEH = 32,
            HUD_HEIGHT = 100;

    Crafty.sprite(32, "ChomperSprites.png", {
        playerRight0: [10, 0],
        playerRight1: [11, 0],
        playerDown0: [10, 1],
        playerDown1: [11, 1],
        playerLeft0: [10, 2],
        playerLeft1: [11, 2],
        playerTop0: [10, 3],
        playerTop1: [11, 3]
    });


    Crafty.c("Wall", {
        init: function() {
            this.addComponent("Collision");
        }
    })
    Crafty.c("Player", {
        _speed: 4,
        _direction: {
            x: 1,
            y: 0
        },
        _directionRequest: null,
        _isMoving: false,
        _score: 0,
        init: function() {
            _this = this;
            this.addComponent("playerRight0, Keyboard, SpriteAnimation, Collision");
            this._movement = {
                x: 0,
                y: 0
            };
            this._speed = {
                x: 4,
                y: 4
            };
            this._movement.x = this._direction.x * this._speed.x;
            this._movement.y = this._direction.y * this._speed.y;
            this.animate("jawMov", 10, 0, 11).animate("jawMov", 15, -1);
            this.bind("EnterFrame", function() {
                if (Crafty.isPaused())
                    return;
                if (!this._isMoving)
                    return this;
                if (this.x % TILEW === 0 && this.y % TILEH === 0) {
                    // Enter new TILE
                    this.trigger("NextTileEntered");
                    if (this._directionRequest) {
                        oldPos = {
                            x: this.x,
                            y: this.y
                        };
                        this.x += this._directionRequest.x * this._speed.x;
                        this.y += this._directionRequest.y * this._speed.y;
                        if (!this.hit("Wall"))
                        {
                            this._direction = this._directionRequest;
                            this._directionRequest = null;
                            this._movement.x = this._direction.x * this._speed.x;
                            this._movement.y = this._direction.y * this._speed.y;
                            this.trigger("NewDirection", this._direction);
                        }
                        this.x = oldPos.x;
                        this.y = oldPos.y;


                    }
                }
                this.x += this._movement.x;
                this.y += this._movement.y;
                this.trigger("Moved", {
                    x: this.x - this._movement.x,
                    y: this.y - this._movement.y
                });

            }), this.bind("KeyDown", function() {
                if (Crafty.isPaused())
                    return;
                if (!this._isMoving) {
                    this._isMoving = true;
                    this.trigger("PlayerStarted");
                }
                if (Crafty.keydown[Crafty.keys['W']] || Crafty.keydown[Crafty.keys['UP_ARROW']])
                    this._directionRequest = {
                        x: 0,
                        y: -1
                    };
                else if (Crafty.keydown[Crafty.keys['S']] || Crafty.keydown[Crafty.keys['DOWN_ARROW']])
                    this._directionRequest = {
                        x: 0,
                        y: 1
                    };
                else if (Crafty.keydown[Crafty.keys['A']] || Crafty.keydown[Crafty.keys['LEFT_ARROW']])
                    this._directionRequest = {
                        x: -1,
                        y: 0
                    };
                else if (Crafty.keydown[Crafty.keys['D']] || Crafty.keydown[Crafty.keys['RIGHT_ARROW']])
                    this._directionRequest = {
                        x: 1,
                        y: 0
                    };
            }),
                    this.bind("Moved", function(oldPos) {
                        if (this.hit("Enemy"))
                            Crafty.scene("GameOver");
                        if (this.hit("Wall")) {
                            this.x = oldPos.x;
                            this.y = oldPos.y;
                        }
                        var feed = this.hit("Feed")
                        if (feed) {
                            for (var i = 0; i < feed.length; i++) {
                                if (feed[i].overlap < -32 / 2) {
                                    feed[i].obj.destroy();
                                    this.trigger("Feed", feed[i].obj);
                                }

                            }
                            //                    this._isMoving = false;
                        }
                    }),
                    this.bind("NewDirection", function(dir) {
                        this.stop(); // Animation stoppen
                        if (dir.y < 0) // up
                            this.animate("jawMov", 10, 3, 11)
                        else if (dir.y > 0) // down
                            this.animate("jawMov", 10, 1, 11)
                        else if (dir.x < 0) // left
                            this.animate("jawMov", 10, 2, 11)
                        else if (dir.x > 0)//right
                            this.animate("jawMov", 10, 0, 11)
                        this.animate("jawMov", 15, -1);
                    }),
                    this.bind("Feed", function(feed) {
                        _this._score += feed.score;
                        Crafty.trigger("ScoreChanged", _this._score);
                    })
        }
    }),
            Crafty.c("Feed", {
                score: 20,
                init: function() {
                    this.addComponent("2D, Canvas, tileSprite30, Collision");
                }
            }),
            Crafty.c("Level", {
                _player: null,
                init: function() {
                    level = this;
                    this.addComponent("2D, Canvas, Color, Keyboard");
                    this.attr({
                        x: 0,
                        y: 0,
                        w: WIDTH,
                        h: HEIGHT
                    });
                    this.color("black");
                    level = this;

                    this.bind("KeyDown", function(e) {
                        if (Crafty.keydown[Crafty.keys['P']])
                            level.pause();
                    })
                },
                pause: function() {
                    overlay = $('#overlay');
                    if (!Crafty.isPaused())
                        overlay
                                .html('').append('<h1>Paused!</h1>')
                                .fadeIn(100, null);
                    else
                        overlay.hide();
                    Crafty.pause();
                },
                _setup: function() {
                },
                _makeLevel: function(file) {
                    this._tiledLevel = Crafty.e("TiledLevel")
                    this._tiledLevel.bind("TiledLevelLoaded", function(tileLoader) {
                        //                Crafty.canvas._canvas.width = tileLoader._width;
                        //                Crafty.canvas._canvas.height = tileLoader._height + HUD_HEIGHT;
                        //                Crafty.canvas._canvas.style.position = 'relative';
                        for (var x = TILEW; x < tileLoader._width; x += TILEW / 2) {
                            for (var y = TILEH; y < tileLoader._height; y += TILEH / 2) {
                                if (x % TILEW > 0 && y % TILEH > 0)
                                    continue;
                                var feed = Crafty.e("Feed").attr({
                                    x: x,
                                    y: y
                                });
                                if (feed.hit("Wall") || feed.hit("Player") || feed.hit("Enemy"))
                                    feed.destroy();
                            }
                        }
                        level.bind("EnterFrame", function(data) {
                            var feeds = Crafty("Feed");
                            if (feeds.length === 0)
                                Crafty.scene("Win");
                        }),
                                $('#score-points').html("0 Punkte");
                        this._player = Crafty("Player");
                        level.bind("ScoreChanged", function(score) {
                            $('#score-points').html(score + " Punkte");
                        })

                        $('#overlay').html('').hide();
                    }).tiledLevel(file);



                },
                _computePos: function(x, y, col, row, bw, bh) {
                    return {
                        x: x + col * bw,
                        y: y + row * bh,
                        //(bh * BOARD_ROWS - (row + 1) * bh)
                    }
                },
            })


    Crafty.scene("Main", function() {
        $('#overlay').html('<h1>Loading...</h1>');
        Crafty.e("Level")._makeLevel("l1.json");
        // Main scene code here
    })

    Crafty.scene("MainMenu", function() {
        var overlay = $('#overlay').html('')
                .append('<h1>Welcome to <b>PaccyPac</b></h1>')
                .append('<p>Control player with <br><b>W, A, S, D</b><br> or <br><b>arrow-keys</b>.</p>')
                .append('<p>Goal is to earn as much as you can points without getting rid of the monsters.</p>');
        startButton = $('<input type="button" value="Start game">');
        startButton.bind('click', null, function() {
            Crafty.scene("Main");
        });
        overlay.append(startButton);
        startButton.focus();
        overlay.show();
    })

    Crafty.scene("GameOver", function() {
        Crafty.scene("MainMenu")
    })
    Crafty.scene("NextLevel", function() {
    })
    Crafty.scene("Win", function() {
        Crafty.scene("MainMenu");
    })



    //    Crafty.debugBar.show();
    Crafty.scene("MainMenu");
});